from rest_framework import status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from backend.models import Answer, Comment
from backend.serializers.answer import AnswerViewSerializer
from backend.serializers.comment import CommentCreateSerializer, CommentUpdateSerializer, CommentViewSerializer
from common.permissions import IsOwner


class CommentCreateView(APIView):

    permission_classes = (IsAuthenticated,)
    def get_serializer(self, *args, **kwargs):
        return CommentCreateSerializer(*args, **kwargs)

    # def get(self, request, answer):
    #     answer = get_object_or_404(Answer, pk=answer)
    #     serializer = AnswerViewSerializer(answer)
    #     return Response(serializer.data)

    def post(self, request, answer):
        answer = get_object_or_404(Answer, pk=answer)
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user, answer=answer)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CommentEditView(APIView):

    permission_classes = (IsOwner,)
    def get_serializer(self, *args, **kwargs):
        return CommentUpdateSerializer(*args, **kwargs)

    def get(self, request, commentid):
        comment = get_object_or_404(Comment, pk=commentid)
        serializer = CommentViewSerializer(comment)
        return Response(serializer.data)

    def put(self, request, commentid):
        comment = get_object_or_404(Comment, pk=commentid)
        serializer = self.get_serializer(instance=comment, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CommentDeleteView(APIView):

    permission_classes = (IsOwner, )

    def delete(self, request, commentid):
        comment = get_object_or_404(Comment, pk = commentid)
        comment.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



