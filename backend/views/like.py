from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from backend.models import Like, Query, Answer
from backend.serializers.like import LikeCreateQuerySerializer, LikeCreateAnswerSerializer, LikesGetQuerySerializer, LikesGetAnswerSerializer
from rest_framework.generics import get_object_or_404


class LikesCreateView(APIView):
    permission_classes = (IsAuthenticated,)

    # def get(self, request, query, answer=None):
    #     if answer:
    #         query = get_object_or_404(Query, pk=query)
    #         answer = get_object_or_404(Answer, pk=answer)
    #         serializer = LikesGetAnswerSerializer(answer)
    #         return Response(serializer.data)
    #     else:
    #         query = get_object_or_404(Query, pk=query)
    #         serializer = LikesGetQuerySerializer(query)
    #         return Response(serializer.data)

    def post(self, request, query, answer=None):
        if answer:
            query = get_object_or_404(Query, pk=query)
            answer = get_object_or_404(Answer, pk=answer)
            type = "A"
            serializer = LikeCreateAnswerSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save(user=request.user, type=type, query=query, answer = answer)
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        else:
            query = get_object_or_404(Query, pk=query)
            type = "Q"
            serializer = LikeCreateQuerySerializer(data=request.data)
            if serializer.is_valid():
                serializer.save(user=request.user, type=type, query=query, answer=None)
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LikesCountView(APIView):

    def get(self, request, query, answer=None):
        if query:
            query = Like.objects.filter(query=query).filter(type="Q").count()
            json = JSONRenderer().render(query)
            return Response(json)
        else:
            answer = Like.objects.filter(answer=answer).filter(type="A").count()
            json = JSONRenderer().render(answer)
            return Response(json)


