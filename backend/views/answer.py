from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.generics import CreateAPIView, UpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from backend.models import Answer, Query

from backend.serializers.answer import AnswerCreateSerializer, AnswerViewSerializer, AnswerEditSerializer

from backend.serializers.query import QueryViewSerializer
from common.permissions import IsOwner

class AnswerCreateView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = AnswerCreateSerializer

    # def get(self, request, query):
    #     query = get_object_or_404(Query, pk=query)
    #     serializer = QueryViewSerializer(query)
    #     return Response(serializer.data)

    def perform_create(self, serializer):
        query_id = self.kwargs['query']
        query = get_object_or_404(Query, pk=query_id)
        serializer.save(user=self.request.user, query=query)


class AnswerRetrieveView(APIView):
    def get(self, request, query, answer):
        answer = get_object_or_404(Answer, pk=answer)
        serializer = AnswerViewSerializer(answer)
        return Response(serializer.data)


class AnswerEditView(APIView):

    permission_classes = (IsOwner,)

    def get_serializer(self, *args, **kwargs):
        return AnswerEditSerializer(*args, **kwargs)

    def get(self, request, query, answer):
        ans = get_object_or_404(Answer, pk=answer)
        serializer = AnswerViewSerializer(ans)
        return Response(serializer.data)

    def put(self, request, query, answer):
        answer = get_object_or_404(Answer, pk=answer)
        serializer = self.get_serializer(instance=answer, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

