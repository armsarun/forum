from rest_framework import status
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from backend.models import Query
from rest_framework.generics import ListAPIView, CreateAPIView, DestroyAPIView, get_object_or_404
from backend.serializers.query import QueryCreateSerializer, QueryEditSerializer, QueryViewSerializer, \
    QueryCloseSerializer
from common.permissions import IsOwner

def slug_generator(year, month, day, title):
    return year + '/' + month + '/' + day + '/' + title

class QueryListView(ListAPIView):
    serializer_class = QueryViewSerializer

    def get_queryset(self):
        query =  Query.objects.all()
        return query


class QueryCreateView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = QueryCreateSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class QueryUpdateView(APIView):

    permission_classes = (IsOwner,)

    def get_serializer(self, *args, **kwargs):
        return QueryEditSerializer(*args, **kwargs)

    def get(self, request, query):
        query = get_object_or_404(Query, pk=query)
        serializer = QueryViewSerializer(query)
        return Response(serializer.data)

    def put(self, request, query):
        query = get_object_or_404(Query, pk=query)
        serializer = self.get_serializer(query, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk):
        query = get_object_or_404(Query, pk=pk)
        serializer = self.get_serializer(query, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class QueryDetailView(APIView):

    def get(self, request, year, month, day, title):
        slug = slug_generator(year, month, day, title)
        query = get_object_or_404(Query, slug=slug)
        serializer = QueryViewSerializer(query)
        return Response(serializer.data)

class QueryCloseView(APIView):

    def get_serializer(self, *args, **kwargs):
        return QueryCloseSerializer(*args,**kwargs)

    def get(self, request, year, month, day, title):
        slug = slug_generator(year, month, day, title)
        query = get_object_or_404(Query, slug=slug)
        serializer = QueryViewSerializer(query)
        return Response(serializer.data)

    def put(self, request, year, month, day, title):
        slug = slug_generator(year, month, day, title)
        query = get_object_or_404(Query, slug=slug)
        serializer = self.get_serializer(query, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)

class QueryDeleteView(DestroyAPIView):
    permission_classes = (IsAdminUser,)
    serializer_class = QueryViewSerializer
