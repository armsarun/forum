from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from backend.models import Category
from rest_framework.generics import ListAPIView, ListCreateAPIView, get_object_or_404
from backend.serializers.category import CategoryCreateSerializer, CategoryEditSerializer, CategoryViewSerializer
from rest_framework.permissions import IsAdminUser


class CategoryListView(ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoryViewSerializer

class CategoryDetailView(APIView):

    def get(self, request, name):

        category = get_object_or_404(Category, name=name)
        serializer = CategoryViewSerializer(category)
        return Response(serializer.data)


class CategoryCreateView(ListCreateAPIView):
    permission_classes = (IsAdminUser,)

    queryset = Category.objects.all()
    serializer_class = CategoryCreateSerializer

    def perform_create(self, serializer):
        serializer.save(user = self.request.user)

class CategoryUpdateView(APIView):

    permission_classes = (IsAdminUser,)

    def get_serializer(self, *args, **kwargs):
        return CategoryEditSerializer(*args, **kwargs)

    def get(self, request, name):
        category = get_object_or_404(Category, name=name)
        serializer  = self.get_serializer(category)
        return Response(serializer.data)

    def put(self, request, name):
        category = get_object_or_404(Category, name=name)
        serializer = self.get_serializer(category, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)

    def patch(self, request, name):
        category = get_object_or_404(Category, name=name)
        serializer = self.get_serializer(category, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)