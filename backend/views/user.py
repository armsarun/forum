from django.http import Http404
from rest_framework import status

from rest_framework.response import Response
from rest_framework.views import APIView
from common.permissions import IsOwner
from backend.models import User
from backend.serializers.user import UserCreateSerializers, UserEditSerializers, UserViewSerializers, SuperUsersCreateSerializers , \
    AdminUserEditViewSerializer, StaffUsersEditViewSerializers, SuperUsersViewSerializers
from rest_framework.generics import ListAPIView, CreateAPIView, get_object_or_404


class UserListView(ListAPIView):
    queryset = User.objects.all()
    def get_serializer_class(self):
        if self.request.user.is_superuser or self.request.user.is_staff:
            return SuperUsersViewSerializers
        else:
            return UserViewSerializers

class UserCreateView(CreateAPIView):
    def get_seriaAPIRequestFactorylizer_class(self):
        if self.request.user.is_superuser or self.request.user.is_staff:
            return SuperUsersCreateSerializers
        else:
            return UserCreateSerializers

class UserUpdateView(APIView):

    permission_classes = (IsOwner,)

    def get_object(self, username):
        try:
            return User.objects.get(username=username)
        except User.DoesNotExist:
            raise Http404

    def get_serializer(self, *args, **kwargs):
        if self.request.user.is_superuser:
            return AdminUserEditViewSerializer(*args, **kwargs)
        elif self.request.user.is_staff:
            return StaffUsersEditViewSerializers(*args, **kwargs)
        else:
            return UserEditSerializers(*args, **kwargs)

    def get(self, request, username):
        user = get_object_or_404(User, username=username)
        serializer = self.get_serializer(user)
        return Response(serializer.data)

    def put(self, request, username):
        user = get_object_or_404(User, username=username)
        serializer = self.get_serializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)

    def patch(self, request, username):
        user = get_object_or_404(User, username=username)
        serializer = self.get_serializer(user, data= request.data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


class UserDetailView(APIView):

    def get_serializer(self, data):
        if self.request.user.is_superuser or self.request.user.is_staff:
            return SuperUsersViewSerializers(data)
        else:
            return UserViewSerializers(data)

    def get(self, request, username):
        user  = get_object_or_404(User, username=username)
        serializer = self.get_serializer(user)
        return Response(serializer.data)

