from django.conf import settings
from django.db import models
from django.db import IntegrityError
from backend.models.query import Query


class Answer(models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    query = models.ForeignKey(Query, related_name='answers', on_delete=None)
    description = models.TextField()
    best_answer = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = (('user', 'query'),)

    def save(self, *args, **kwargs):
        # check query has best answer
        if self.best_answer:
            best_answer_count = Answer.objects.filter(query=self.query).filter(best_answer=True).count()
            if best_answer_count == 1:
                raise IntegrityError("Best answer exist")
            else:
                super(Answer, self).save()
        super(Answer, self).save()

    def __str__(self):
        return self.description
