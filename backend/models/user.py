from __future__ import unicode_literals

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models


class CustomUserManager(BaseUserManager):
    use_in_migrations = True

    # custom normalize email and return exception if email is invalid
    def custom_normalize_email(self, email):
        try:
            email_name, domain_part = email.strip().rsplit('@', 1)
        except ValueError as e:
            return "Email is Invalid"
        else:
            email = '@'.join([email_name, domain_part.lower()])
        return email

    def _create_user(self, username, email, password, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not email and not username:
            raise ValueError('The given username must be set and email to be set')

        email = self.custom_normalize_email(email)
        username = self.model.normalize_username(username)
        user = self.model(username=username, email=email, **extra_fields)
        user.set_password(password)  # set user password with salt encryption
        user.save(using=self._db)
        return user

    def create_user(self, username, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, email, password, **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username, email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=254, unique=True, )
    email = models.EmailField(max_length=254, unique=True)
    first_name = models.CharField(max_length=254)
    last_name = models.CharField(max_length=254, null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    gender = models.CharField(max_length=100, blank=True)
    avatar = models.ImageField(blank=True, null=True)
    password = models.CharField(max_length=254)
    country = models.CharField(max_length=254, null=True)
    date_joined = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    # cutstom manager to create new with username and email
    objects = CustomUserManager()

    # Make the username field as required field so it can show while creating superuser
    REQUIRED_FIELDS = ["username"]

    # Custom username field

    USERNAME_FIELD = "email"


    def get_full_name(self):
        fullname = "{}{}".format(self.first_name, self.last_name)
        return fullname

    def __str__(self):
        return self.username

    def get_short_name(self):
        return self.first_name