from django.conf import settings
from django.db import models

from backend.models.query import Query
from backend.models.answer import Answer


class Like(models.Model):
    TYPES = (
        ("Q", "Question Likes"),
        ("A", "Answer Likes")
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    query = models.ForeignKey(Query, related_name='query_likes', on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, related_name='answer_likes', null=True, on_delete=models.CASCADE)
    type = models.CharField(choices=TYPES, max_length=1)

    def __str__(self):
        return self.type

    def save(self, *args, **kwargs):

        try:
            like_find = Like.objects.get(user=self.user, query=self.query, answer=self.answer, type=self.type)
            like_find.delete()

        except Like.DoesNotExist:
            super(Like, self).save(*args, **kwargs)
