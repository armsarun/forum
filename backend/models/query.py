from django.conf import settings
from django.db import models
from django.utils.text import slugify

from backend.models.category import Category


class Query(models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=None)  # refer on delete
    title = models.CharField(max_length=255)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    is_closed = models.BooleanField(default=False)
    comment = models.TextField()
    slug = models.SlugField()   # change to auto slug

    def save(self, *args, **kwargs):

        if not self.id:
            #save query to get the created field first
            super(Query, self).save()
            #create slug for newly created field
            self.slug = '{year}/{month}/{day}/{title}'.format(
                            year = self.created.year,
                            month = self.created.month,
                            day = self.created.day,
                            title = slugify(self.title))
            #update with slug field
            super(Query, self).save()
        else:
            super(Query, self).save()

    def __str__(self):
        return self.title

