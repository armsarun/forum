from django.conf.urls import url


from backend.views.answer import AnswerCreateView, AnswerEditView, AnswerRetrieveView
from backend.views.category import CategoryListView, CategoryUpdateView, CategoryCreateView, CategoryDetailView
from backend.views.comment import CommentCreateView, CommentEditView, CommentDeleteView
from backend.views.like import LikesCreateView, LikesCountView
from backend.views.query import QueryListView, QueryCreateView, QueryUpdateView, QueryDeleteView, \
    QueryDetailView, QueryCloseView
from backend.views.user import UserCreateView, UserDetailView, UserListView, UserUpdateView

user = [
    url(r'^signup/', UserCreateView.as_view()),
    url(r'^users/(?P<username>\w+)/$', UserDetailView.as_view()),
    url(r'^users/$', UserListView.as_view()),
    url(r'^user/(?P<username>\w+)/$', UserUpdateView.as_view())
]

category = [
    url(r'^create/category', CategoryCreateView.as_view()),
    url(r'^categories/$', CategoryListView.as_view()),
    url(r'^categories/(?P<name>\w+)/$', CategoryDetailView.as_view()),
    url(r'^category/(?P<name>\w+)/$', CategoryUpdateView.as_view()),
]

query = [
    url(r'^$', QueryListView.as_view()),
    url(r'^(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/(?P<title>[-\w]+)/$', QueryDetailView.as_view()),
    url(r'^update/(?P<query>\d+)/$', QueryUpdateView.as_view()),
    url(r'^create/query/', QueryCreateView.as_view()),
    url(r'^delete/query/(?P<query>\d+)/$', QueryDeleteView.as_view()),
    url(r'^close/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/(?P<title>[-\w]+)/$', QueryCloseView.as_view())
]

answer = [
    url(r'^create/answer/(?P<query>\d+)/$', AnswerCreateView.as_view()),
    url(r'^update/(?P<query>\d+)/(?P<answer>\d+)/$', AnswerEditView.as_view()),
    url(r'^(?P<query>\d+)/(?P<answer>\d+)/$', AnswerRetrieveView.as_view())
]

comment = [
    url(r'^create/comment/(?P<answer>\d+)/$',  CommentCreateView.as_view()),
    url(r'^update/comment/(?P<commentid>\d+)/$', CommentEditView.as_view()),
    url(r'^delete/comment/(?P<commentid>\d+)/$', CommentDeleteView.as_view())
]

likes = [
    url(r'^create/like/(?P<query>\d+)/$', LikesCreateView.as_view()),
    url(r'^create/like/(?P<query>\d+)/(?P<answer>\d+)/$', LikesCreateView.as_view()),
    url(r'^likes/(?P<query>\d+)/$', LikesCountView.as_view()),
    url(r'^likes/(?P<query>\d+)/(?P<answer>\d+)/$', LikesCountView.as_view()),
]



urlpatterns = user + category + query + answer + likes + comment
