from backend.models import User, Query
from backend.serializers.query import QueryViewSerializer, QueryCreateSerializer
from backend.tests.views.commonsetup import CommonSetup
from backend.views.query import QueryCreateView, QueryDetailView, QueryListView, QueryUpdateView, QueryCloseView, QueryDeleteView


class QueryViewTestCase(CommonSetup):

    def setUp(self):
        super().setUp()
        self.query = Query.objects.all()

    def test_createquery(self):
        request = self.client.post('/create/query/')
        view = QueryCreateView.as_view()
        serializer = QueryCreateSerializer()
        response = view(request)
        # self.assertEqual(response.data, serializer.data)

    def test_querylist(self):
        request = self.client.get('/users/')
        view = QueryListView.as_view()
        serializer = QueryViewSerializer(self.query, many=True)
        response = view(request)
        print(response.data)
        self.assertEqual(response.data, serializer.data)
