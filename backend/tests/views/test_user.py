from rest_framework.test import force_authenticate

from backend.models import User
from backend.serializers.user import UserViewSerializers, SuperUsersViewSerializers
from backend.tests.views.commonsetup import CommonSetup
from backend.views.user import UserListView, UserUpdateView, UserDetailView

class UserViewTestCase(CommonSetup):

    def test_userlistview(self):
        request = self.client.get('/users/')
        view = UserListView.as_view()
        serializer = UserViewSerializers(self.users, many=True)
        response = view(request)

        self.assertEqual(response.data, serializer.data)

    def test_userlistadminview(self):
        request = self.client.get('/users/')
        force_authenticate(request, user=self.adminuser)
        view = UserListView.as_view()
        response = view(request)

        serializer = SuperUsersViewSerializers(self.users, many=True)

        self.assertEqual(response.data, serializer.data)

    def test_userupdateview(self):
        request = self.client.put('/update/testuser/', {'username':'testuser'})
        view = UserUpdateView.as_view()
        reponse = view(request, username=self.user.username)
        self.assertEqual(reponse.status_code, 401)

    def test_userupdatewithlogin(self):
        request = self.client.patch('/update/testuser/',{'username':'test'})
        view = UserUpdateView.as_view()
        force_authenticate(request, user=self.user)
        response = view(request, username=self.user.username)
        self.assertEqual(response.status_code, 200)
        update = User.objects.get(username='test')
        self.assertEqual(update.username, 'test')

    def test_userdetailview(self):
        request = self.client.get('/users/testuser')
        view = UserDetailView.as_view()
        response = view(request, username=self.user.username)

        user = User.objects.get(username='testuser')
        serializer = UserViewSerializers(user)
        self.assertEqual(response.data, serializer.data)

    def test_userdetailviewadmin(self):
        request = self.client.get('/users/testuser')
        view = UserDetailView.as_view()
        force_authenticate(request, self.adminuser)
        response = view(request, username=self.user.username)

        user = User.objects.get(username='testuser')
        serializer = SuperUsersViewSerializers(user)
        self.assertEqual(response.data, serializer.data)

    def test_usersignup(self):
        pass