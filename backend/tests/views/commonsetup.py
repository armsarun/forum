from django.test import TestCase
from rest_framework.test import APIRequestFactory

from backend.models import User


class CommonSetup(TestCase):

    def setUp(self):
        User.objects.create_user(username="testuser", email="sample@example.com", password="admin@1234")
        User.objects.create_superuser(username="testadmin", email="admin@sample.com", password="admin@1234")
        self.user = User.objects.get(username="testuser")
        self.adminuser = User.objects.get(username="testadmin")
        self.client = APIRequestFactory()
        self.users = User.objects.all()