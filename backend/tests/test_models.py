# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.contrib.auth.hashers import check_password
from django.db import IntegrityError
from django.test import TestCase, Client
from backend.models import User, Category, Query, Answer, Like


class CommonSetupMixin(TestCase):
    def setUp(self):
        # Requestfactory for login test
        self.c = Client()
        self.today = datetime.datetime.today()

        # user create
        User.objects.create_user(username="admin", email="admin@sample.com", password="admin")
        self.test_user = User.objects.get(username="admin")

        User.objects.create_user(username="staff", email="staff@exampel.com", password="admin")
        self.test_user1 = User.objects.get(username="staff")

        # category create
        Category.objects.create(user=self.test_user, name="test_category", description="sample category for testing")
        self.category = Category.objects.get(name="test_category")

        # Query Create
        Query.objects.create(user=self.test_user, category=self.category)
        Query.objects.create(user=self.test_user, category=self.category, title="Test query", description="sample1 query")
        Query.objects.create(user=self.test_user, category=self.category, title="Test2",
                             description="sample query")
        self.query = Query.objects.get(title="Test query")
        self.query1 = Query.objects.get(title="Test2")


        Answer.objects.create(user=self.test_user, query=self.query, description="sample answer for query")
        self.answer = Answer.objects.get(description="sample answer for query")


class UsermodelTestcases(CommonSetupMixin):

    def test_passwordisnotPlainText(self):
        password = self.test_user.password
        assert ("admin" != password)

    def test_password_hash(self):
        hashed_password = check_password("admin", self.test_user.password)
        assert (hashed_password == True)

    def test_userlogin_withusername(self):
        login = self.c.login(username=self.test_user.username, password="admin")
        assert (login == True)

    def test_userlogin_withemail(self):
        login = self.c.login(email=self.test_user.email, password="admin")
        assert (login == True)

    def test_superusercreate(self):
        User.objects.create_superuser("super_admin", "superadmin@example.com", "admin@1234")
        super_user = User.objects.get(username="super_admin")
        assert (super_user.is_staff == True)
        assert (super_user.is_superuser == True)

class CategoryTestCase(CommonSetupMixin):
    def test_categorycreated(self):
        assert (self.category.created.today() == self.today.date())

class QueryTestCase(CommonSetupMixin):
    def test_querycreated(self):
        assert (self.query.created.date() == self.today.date())

    def test_queryupdated(self):
        update = Query.objects.filter(title="Test query")
        update.update(title = "new title")
        new_update = Query.objects.get(title="new title")
        print (new_update.title)
        assert (new_update.updated.today().time() != new_update.created.time())

class AnswerTestCase(CommonSetupMixin):

    def test_createanswer(self):

        assert (self.answer.user == self.test_user)
        self.sameuser = Answer.objects.create(user=self.test_user1, query=self.query, description="best answer")
        self.answer1 = Answer.objects.get(description="best answer")
        assert (self.answer1.user == self.test_user1)

        # user and query should be unique
        with self.assertRaises(IntegrityError):
            self.sameuser = Answer.objects.create(user=self.test_user, query=self.query, description="sample unique test")

    def test_verifycorrectanswer(self):

        Answer.objects.create(user=self.test_user1, query=self.query, description="best answer", best_answer=True)
        answer = Answer.objects.filter(query=self.query).filter(best_answer=True).values_list('query', flat=True).count()
        assert (answer == 1)

        # test to save two best answer
        with self.assertRaises(IntegrityError):
            Answer.objects.create(user=self.test_user1, query=self.query, description="best answer", best_answer=True)

class LikesTestCase(CommonSetupMixin):
    def test_createlikeanddeleteidexist(self):
        Like.objects.create(user=self.test_user, query=self.query, answer=self.answer, type="Q")
        Like.objects.create(user=self.test_user, query=self.query, answer=self.answer, type="Q")

        assert (0 == Like.objects.filter(type="Q").count())
