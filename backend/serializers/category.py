from backend.models import Category
from rest_framework import serializers

class CategoryViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        exclude = ('user',)


class CategoryCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        exclude= ('created', 'user')

class CategoryEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        exclude = ('created',)
        read_only_fields = ('user',)