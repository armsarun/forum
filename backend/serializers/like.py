from backend.models import Like
from rest_framework import serializers

from backend.serializers.query import QueryViewSerializer


class LikeCreateQuerySerializer(serializers.ModelSerializer):

    class Meta:
        model = Like
        exclude = ('type', 'answer', 'user', 'query')

class LikeCreateAnswerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Like
        exclude = ('type', 'answer', 'user', 'query')

class LikesGetQuerySerializer(serializers.Serializer):

    query = serializers.IntegerField(read_only=True)

class LikesGetAnswerSerializer(serializers.Serializer):
    answer = serializers.IntegerField(read_only=True)