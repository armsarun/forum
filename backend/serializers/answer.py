from backend.models import Answer, Like
from rest_framework import serializers

from backend.serializers.comment import CommentViewSerializer


class AnswerCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Answer
        fields = ('description', )

class AnswerViewSerializer(serializers.ModelSerializer):

    answer_likes =  serializers.SerializerMethodField(read_only=True)
    comments = CommentViewSerializer(many=True)

    # Relation model source field show and add these fields to extra fields to show

    # query_title = serializers.CharField(read_only=True, source='query.title')
    # query_description = serializers.CharField(read_only=True, source='query.description')

    class Meta:

        model = Answer
        fields = '__all__'
        extra_fields = ['comments', 'answer_likes']

    def get_answer_likes(self, obj):
        answer = Like.objects.filter(answer=obj.pk).filter(type="A").count()
        return answer

class AnswerEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('description', )
