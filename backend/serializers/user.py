from rest_framework import serializers

from backend.models import User

class UserViewSerializers(serializers.ModelSerializer):

    class Meta:
        model = User
        exclude = ('is_active', 'is_superuser', 'password', 'groups', 'user_permissions')
        depth = 1

class UserCreateSerializers(serializers.ModelSerializer):

    class Meta:
        model = User
        exclude = ('is_staff', 'is_active', 'is_superuser', 'last_login', 'date_joined')
        depth = 1


class UserEditSerializers(serializers.ModelSerializer):

    class Meta:
        model = User
        exclude = ('last_login', 'is_staff', 'is_active', 'date_joined', 'is_superuser', 'groups', 'user_permissions')
        depth = 1
#Super User serializers

class SuperUsersCreateSerializers(serializers.ModelSerializer):

    class Meta:
        model = User
        exclude = ('last_login', 'date_joined')

class AdminUserEditViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        read_only_fields = ('last_login', 'date_joined')
        fields='__all__'


class StaffUsersEditViewSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        read_only_fields = ('last_login', 'date_joined')
        exclude = ('is_staff', 'is_active', 'is_superuser', 'password')


class SuperUsersViewSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ('password',)