from rest_framework import serializers

from backend.models import Comment


class CommentCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = ('description', )

class CommentUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = ('description', )

class CommentViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = '__all__'
