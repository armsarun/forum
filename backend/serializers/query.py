from backend.models import Query, Like

from rest_framework import serializers

from backend.serializers.answer import AnswerViewSerializer



class QueryViewSerializer(serializers.ModelSerializer):

    # only answers display

    # answers = serializers.StringRelatedField(many=True)

    # Nested Reslationship show
    answers = AnswerViewSerializer(many=True)

    # custom field to reterive query in serializer
    query_likes =  serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Query
        fields = '__all__'
        extra_fields = ['answers', 'query_likes']

    def get_query_likes(self, obj):
        query = Like.objects.filter(query=obj.pk).filter(type="Q").count()
        return query

class QueryCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Query
        exclude = ('user', 'slug', 'created', 'updated', 'is_closed', 'comment')


class QueryEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = Query
        read_only_fields = ('slug', 'user')
        exclude = ('comment',)


class QueryCloseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Query
        fields = ('is_closed', 'comment')

