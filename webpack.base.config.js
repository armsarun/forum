const path = require('path');
const webpack = require('webpack');
const BundleTrackerPlugin = require('webpack-bundle-tracker');
const VENDOR_LIBS = [
    'react', 'react-dom', "@material-ui/core", "@material-ui/icons",
];

module.exports = {

    // base directory(absolute path) for resolving the entry
    context: __dirname,

    // the entry point for the app

    //current direcotry doesn't require extension

    //resolve section will mention the extension

    // entry: './frontend/assets/js/index',
    entry: {
        bundle: './frontend/assets/js/index',
        vendor: VENDOR_LIBS
    },

    output: {

        //compiled bundle storing path
        path: path.resolve('./frontend/assets/bundles'),
        //name convetion for webpack how to store the filenames
        // filename: 'bundle.js',
        filename: '[name].[hash].js'
    },

    plugins: [
        //add all plugins here
    ],
    module: {
        //regex tells webpack following loaders on all
        rules: [
            {
                test: /\.js$/,
                exclude: /node-modules/,
                use: {
                    loader: 'babel-loader',
                }
            }
        ]
    },

    resolve: {
        //extesnion should be used to resolve module
        extensions: ['*', '.js', '.jsx'],
        //tells webpack where to look modules
        modules: [
            path.resolve(__dirname, 'node_modules'),
        ],
    }

};
