import React, {Component} from 'react';
import GridCreatorMixin from "../common/Grid";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import {Theme} from "../theme.config";
import {FooterSocialLinks, FormColContainer, LinkContainer, TextContainer, TextContainerPara} from "../styles/home";
import {CustomLink, SignupInput, SignupLabel} from "../styles/common";
import InputLabel from "@material-ui/core/es/InputLabel/InputLabel";
import Input from "@material-ui/core/es/Input/Input";
import Button from "@material-ui/core/Button";

//welcome container
const hometext = () => {
    return (
        <TextContainer>
            <TextContainerPara>
                <Typography variant="h4" gutterBottom>
                    Build for SAP Professional.
                    Collabrate, Share and Build.
                </Typography>
                <Typography variant="h5" gutterBottom>
                    Immense your connection rather than the bugs and issues. Connect with the best developers.
                </Typography>
            </TextContainerPara>
        </TextContainer>
    )
};

const signup = () => {
    return (
        <FormColContainer>
            <form>
                <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="username">Username</InputLabel>
                    <Input id="username" name="username"/>
                </FormControl>
                <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="email">Email</InputLabel>
                    <Input id="email" name="email" autoComplete="email"/>
                </FormControl>
                <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="password">Password</InputLabel>
                    <Input name="password" type="password" id="password"/>
                </FormControl>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="secondary">
                    Sign up
                </Button>

            </form>
            <Typography variant='h6'>or</Typography>
            <FooterSocialLinks>
                <CustomLink to={''}>Google</CustomLink>
                <CustomLink to={''}>Facebook</CustomLink>
                <CustomLink to={''}>Twitter</CustomLink>
            </FooterSocialLinks>
        </FormColContainer>
    )
};
// welcome container end

// app list
const links = (content) => {
    return (
        <LinkContainer>
            {content}
        </LinkContainer>
    )
};
// app list end

//grid setup
const container = {
    container: true,
    spacing: 32
};
const welcomesubgrid =
    [
        {
            classes: {
                item: true,
                xs: 8

            },
            content: hometext(),
            key: 'welcome-1'
        },
        {
            classes: {
                item: true,
                xs: 4

            },
            content: signup(),
            key: 'welcome-2'
        }
    ];

const linksubgrid = [
    {
        classes: {
            item: true,
            xs: 4,
        },
        content: links(<h1>Link1</h1>),
        key: 'links-1'
    }, {
        classes: {
            item: true,
            xs: 4,
        },
        content: links(<h1>Link1</h1>),
        key: 'links-2'

    }, {
        classes: {
            item: true,
            xs: 4,
        },
        content: links(<h1>Link1</h1>),
        key: 'links-3'
    }
];

// grid setup end

class Home extends Component {
    render() {
        return (
            <React.Fragment>
                <GridCreatorMixin background={Theme.palette.primary.light} opacity = {0.8} padding={'5px'} main={container}
                                  sub={welcomesubgrid}/>
                <GridCreatorMixin main={container} sub={linksubgrid}/>
            </React.Fragment>
        )
    }
}

export default Home;