import {Toolbar, Button} from "@material-ui/core";
import React from "react";
import {CustomLink} from "../styles/common";
import {PATH} from "../router/path";

const MENU_ITEMS = [
    {
        label: "Forum",
        path: PATH.FORUM_PAGE,
        exact: true
    },
    {
        label: "News",
        path: PATH.NEWS_PAGE,
        exact: true
    },
    {
        label: "Blog",
        path: PATH.HOME_PAGE,
        exact: true
    }
];

export const MyNavLinks = () => (
    <Toolbar>
        {MENU_ITEMS.map(items => (
            <CustomLink
                to={items.path}
                exact={items.exact}
                key={items.path}
            >
                {items.label}
            </CustomLink>
        ))}
    </Toolbar>
);
