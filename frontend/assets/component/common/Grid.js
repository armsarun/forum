import React, {Component} from 'react';
import {GridLayout} from "../styles/common";
import Grid from "@material-ui/core/Grid";
import PropTypes from 'prop-types';


function Creategrid(props) {
    return (
        <React.Fragment>
            {props.list.map((item) => (
                <GridLayout key={item.key}{...item.classes}> {item.content} </GridLayout>
            ))}
        </React.Fragment>
    )
}

const GridCreatorMixin = (props) => {

    return (
        <React.Fragment>
            <GridLayout opacity = {props.opacity? props.opacity:1} padding = {props.padding? props.padding:0} background = {props.background? props.background : 'inherit'} {...props.main}>
                <Creategrid list={props.sub}/>
            </GridLayout>
        </React.Fragment>
    )

};


GridCreatorMixin.propTypes = {
    main: PropTypes.object,
    sub: PropTypes.array
};


GridCreatorMixin.defaultProps = {
    main: {
        container: true,
        spacing: 24
    }
};

export default GridCreatorMixin;

