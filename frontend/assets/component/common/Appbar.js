import React from 'react';
import PropTypes from 'prop-types';
import {createMuiTheme, withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import {Theme} from "../theme.config";

import {InnerMenu, Logo, NavAlign} from '../styles/appbar'
import {fade} from "@material-ui/core/es/styles/colorManipulator";
import {MyNavLinks} from "./MenuList";
import {CustomLink} from "../styles/common";
import {PATH} from "../router/path";
import {HomeMenu} from "../router/router";

const theme = createMuiTheme(Theme);

const styles = {
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing.unit,
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: 250,
            '&:focus': {
                width: 350,
            },
        },
    },
};

function ButtonAppBar(props) {
    const {classes} = props;
    return (
        <AppBar position="relative">
            <NavAlign>
                <CustomLink to={HomeMenu.path} exact={HomeMenu.exact}>
                    <Logo/>
                    <Typography variant="h6" color="inherit">
                        {HomeMenu.label}
                    </Typography>
                </CustomLink>
                <InnerMenu>
                    <MyNavLinks/>

                    <div className={classes.search}>
                        <div className={classes.searchIcon}>
                            <SearchIcon/>
                        </div>
                        <InputBase
                            placeholder="Search…"
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                        />
                    </div>
                    <Button color="inherit">Login</Button>
                </InnerMenu>

            </NavAlign>
        </AppBar>
    );
}

ButtonAppBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ButtonAppBar);
