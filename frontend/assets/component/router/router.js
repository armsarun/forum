import React from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import DefaultLayout from './layout/defaultLayout';
import Home from '../home/Home';
import {PATH} from './path';
import Forum from '../forum/index';
import News from '../news/index';

//const route for home menu
export const HomeMenu = {
    label: "SAP Community",
    path: PATH.HOME_PAGE,
    exact: true
};

const AppRoute = ({component: Component, layout: Layout, ...rest}) => {
    return (
        <Route
            {...rest}
            render={matchProps => (
                <Layout>
                    <Component {...matchProps} />
                </Layout>
            )}
        />
    );
};

const ROUTES = [
    {
        layout: DefaultLayout,
        component: Home,
        path: PATH.HOME_PAGE,
        exact: true
    },
    {
        layout: DefaultLayout,
        component: Forum,
        path: PATH.FORUM_PAGE,
        exact: true
    },
    {
        layout: DefaultLayout,
        component: News,
        path: PATH.NEWS_PAGE,
        exact: true
    }
];

const CoreRoute = () => (
    <Router>
        <Switch>
            {ROUTES.map(route => (
                <AppRoute
                    key={route.path}
                    path={route.path}
                    component={route.component}
                    layout={route.layout}
                    exact={route.exact}
                />
            ))}
        </Switch>
    </Router>
);

export default CoreRoute;
