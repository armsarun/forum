import React from 'react';
import AppBar from '../../common/Appbar';
import {CustomLink, FixedBottom, GlobalStyle} from '../../styles/common';
import GridCreatorMixin from '../../common/Grid';
import {Theme} from "../../theme.config";
import {FooterContainer, FooterLink, Footerlinks, FooterSocialLinks} from "../../styles/home";
import Typography from "@material-ui/core/es/Typography/Typography";
import {CopyrightLogo, Logo} from "../../styles/appbar";
import {HomeMenu} from "../router";

//footer
const footer_content1 = () => {
    return (
        <FooterContainer>
            <CustomLink to={''}>
                <Logo/>
                <Typography variant="subtitle1" color="inherit">
                    SAP Community
                </Typography>
            </CustomLink>

        </FooterContainer>
    )
};
const footer_content2 = () => {
    return (
        <Footerlinks>
            <FooterContainer>
                <FooterLink>
                    <CustomLink to={''}>About</CustomLink>
                    <CustomLink to={''}>Contact Us</CustomLink>
                    <CustomLink to={''}>Privacy and policy</CustomLink>
                </FooterLink>
            </FooterContainer>
            <FooterContainer>
                <Typography variant="subtitle1" color='inherit'>Follow us</Typography>
                <FooterSocialLinks>
                    <CustomLink to={''}>Google</CustomLink>
                    <CustomLink to={''}>Facebook</CustomLink>
                    <CustomLink to={''}>Twitter</CustomLink>
                </FooterSocialLinks>
            </FooterContainer>
        </Footerlinks>
    )
};
const footer_content3 = () => {
    return (
        <FooterContainer>
            <Typography variant="subtitle1" color='inherit'>
                Site design <CopyrightLogo/> 2019 ArunKumar
            </Typography>
        </FooterContainer>
    )
};
//footer end


const container = {
    container: true,
    spacing: 16,
};

const footersubgrid = [
    {
        classes: {
            item: true,
            xs: 2
        },
        content: footer_content1(),
        key: 'footer-1'
    }, {
        classes: {
            item: true,
            xs: 5
        },
        content: footer_content2(),
        key: 'footer-2'

    },
    {
        classes: {
            item: true,
            xs: 5
        },
        content: footer_content3(),
        key: 'footer-3'

    }
];

const DefaultLayout = ({children, ...rest}) => (
    <React.Fragment>
        <GlobalStyle/>
        <AppBar/>
        {children}
        <FixedBottom>
            <GridCreatorMixin padding={'10px'} background={Theme.palette.primary.dark} main={container} sub={footersubgrid}/>
        </FixedBottom>
    </React.Fragment>
);

export default DefaultLayout;
