import React from 'react';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import {Theme} from "./theme.config";
import Route from './router/router';

const theme = createMuiTheme(Theme);

const App = () => (
    <MuiThemeProvider theme={theme}>
        <Route />
    </MuiThemeProvider>
);

export default App;