import {FlexdivRow} from '../styles/common'
import styled from "styled-components";
import ForumIcon from "@material-ui/icons/Forum";
import Copyrigt from "@material-ui/icons/Copyright"

export const NavAlign = styled(FlexdivRow)`
margin: 10px 24px 10px 24px;
align-items:center;
position:relative;
`;

export const InnerMenu = styled(FlexdivRow)`
width: 87%;
align-items: center;
justify-content: space-around;
`;

export const Logo = styled(ForumIcon)`
color:#fff;
margin-right: 10px;
`;

export const CopyrightLogo = styled(Copyrigt)`
color:#fff;
font-size: 15px !important;
`;