import React from 'react'
import {createGlobalStyle} from 'styled-components';
import styled from "styled-components";
import Grid from "@material-ui/core/Grid";
import { NavLink } from "react-router-dom";



export const GlobalStyle = createGlobalStyle`
body{
  margin:0 auto;
  height:100%;
  //hide the scrollbar
  overflow-y: hidden;
  overflow-x: hidden;
}`;

export const FLexdivMixin = styled.div`
display:flex;
`;

export const FlexspanMixin = styled.span`
display:flex;
`;

export const FlexdivCol = styled(FLexdivMixin)`
flex-direction: column;
`;

export const FlexspanCol = styled(FlexspanMixin)`
flex-direction: column;
`;

export const FlexdivRow = styled(FLexdivMixin)`
flex-direction: row;
`;

export const FlexspanRow = styled(FlexspanMixin)`
flex-direction: row;
`;


export const GridLayout = styled(Grid)`
    background:${props => props.background};
    padding :${props => props.padding};
    opacity:${props => props.opacity}
`;

export const FixedBottom = styled.div`
    position: absolute;
    bottom: 0;
    width: 100%;
`;

const NewNavLink = styled(NavLink)`
    font-family: "Roboto", "Helvetica", "Arial", sans-serif;
    font-weight: 500;
    font-size: 0.875rem;
    text-decoration: none;
    color: inherit;
    text-transform: uppercase;
    padding:8px 16px;
    &:hover{
        background-color: rgba(0, 0, 0, 0.08);
    }
`;

export const CustomLink = ({...props}) => <NewNavLink {...props}/>;