import styled from 'styled-components'
import {FlexdivCol, FlexdivRow} from "./common";
import Button from "@material-ui/core/Button";

//footer css
export const FooterContainer = styled(FlexdivCol)`
    color:#fff;
    justify-content: space-around;
    align-items: center;
    line-height:1.8;
`;

export const FooterLink = styled.div`
    align-items: initial;
`;

export const Footerlinks = styled(FlexdivRow)`
    justify-content: space-around;
`;

export const FooterSocialLinks = styled(FlexdivRow)`
justify-content: center;
margin-top: 10px;
& > a{
margin-left: 10px;
}

`;
//footer css


// content page start

//welcome container

//form continaer
export const FormColContainer = styled(FlexdivCol)`
    background: #fff;
    padding: 40px;
    margin: 30px;
    width: 60%;
    & > form > button {
        margin:34px 0px 28px 0px;
    }
    & > form input{
       color:#222;
    }
    & > h6{
    text-align:center;
        color:#222;
    }
`;

//link container
export const LinkContainer = styled(FlexdivCol)`
  align-items:center;
  justify-content: space-around;
`;
//form container end


// textcontainer start
export const TextContainer = styled(FlexdivCol) `
  height: 100%;
  align-items:center;
  justify-content:center;
`;

export const TextContainerPara = styled(FlexdivCol)`
  align-items: flex-start;
  width: 50%;
`;
// textconttainer end
//welcome container end


//content page end

