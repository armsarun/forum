from rest_framework import permissions
from rest_framework.permissions import BasePermission


class IsOwner(BasePermission):
    """
        Allow only Owners to edit object
    """

    # check is Authenticated
    def has_permission(self, request, view):
        """
           Access
        """

        return (request.method in permissions.SAFE_METHODS or
            request.user and
            request.user.is_authenticated)

    def has_object_permission(self, request, view, obj):
        try:
            if request.method in permissions.SAFE_METHODS:
                return True

            return obj.user == request.user or request.user.is_superuser

        except:

            if request.method in permissions.SAFE_METHODS:
                return True

            return obj.username == request.user.username or request.user.is_superuser
