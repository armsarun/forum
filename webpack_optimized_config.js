var path = require('path')
var webpack = require('webpack')
var BundleTrackerPlugin = require('webpack-bundle-tracker')
var HTMLWebpackpluglin = require('html-webpack-plugin')

var VENDOR_LIBS=['react', 'react-dom'];

module.exports = {

    // base directory(absolute path) for resolving the entry
    context: __dirname,

    // the entry point for the app

    //current direcotry doesn't require extension

    //resolve section will mention the extension

    //entry: './frontend/assets/js/index',
    entry: {
        bundle: './frontend/assets/js/index',
        vendor: VENDOR_LIBS
    },

    output: {
        //compiled bundle storing path
        path: path.resolve('./frontend/assets/bundles'),
        //name convetion for webpack how to store the filenames
        filename: '[name].[chunkhash].js',

    },

    plugins: [

        //common chunk plugin handle the vendor and appliction js seprate file

        //Instead of building vendor everytime in build it build the vendor whenever it updates in package.json

        //mainfest it stores the name of vendor and other bundle file names. if webpack find the changes in filename of
        //current and previous bundle names. it updates the filename and the cache to browser
        new HTMLWebpackpluglin({
            template: ''
        }),
        new webpack.optimize.CommonsChunkPlugin({
            names: ['vendor', 'manifest']
        }),
        // store the data about the bundle
        new BundleTrackerPlugin({filename: './webpack-stats.json'}),

    ],
    module: {
        //regex tells webpack following loaders on all
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node-modules/,
                loader:"babel-loader",
                options:{
                    presets:["react"]
                }
            }
        ]
    },

    resolve: {
        //extesnion should be used to resolve module
        extensions:['*', '.js', '.jsx'],
        //tells webpack where to look modules
        modules: ['node_modules'],

    }

}