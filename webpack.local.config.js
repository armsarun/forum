const path = require('path');
const webpack = require('webpack');
const BundleTrackerPlugin = require('webpack-bundle-tracker');
let config = require('./webpack.base.config.js');

config.mode = 'development';

config.devtool = 'inline-source-map';
config.output.publicPath = 'http://localhost:3000/assets/bundles/';

config.plugins = [
    new webpack.HotModuleReplacementPlugin(),
    // store the data about the bundle
    new BundleTrackerPlugin({filename: './webpack-stats.json'}),
];



module.exports = config;

